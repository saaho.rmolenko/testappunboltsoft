FROM alpine
RUN apk add --update openjdk11

ADD target/TestAppUnboltSoft-1.0-SNAPSHOT.jar TestAppUnboltSoft-1.0-SNAPSHOT.jar

EXPOSE 8080
EXPOSE 27017

ENTRYPOINT ["java", "-jar", "TestAppUnboltSoft-1.0-SNAPSHOT.jar"]