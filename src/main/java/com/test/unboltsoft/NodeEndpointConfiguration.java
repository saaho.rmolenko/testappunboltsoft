package com.test.unboltsoft;

import com.test.unboltsoft.handlers.NodeDescHandler;
import com.test.unboltsoft.handlers.NodeHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicate;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class NodeEndpointConfiguration {

    private static RequestPredicate i(RequestPredicate target) {
        return new CaseInsensitiveRequestPredicate(target);
    }

    @Bean
    RouterFunction<ServerResponse> routes(NodeHandler handler, NodeDescHandler descHandler) {
        return route(i(GET("/nodes")), handler::all)
                .andRoute(i(POST("/nodes")), handler::create)
                .andRoute(i(GET("/nodes/description")), descHandler::all)
                .andRoute(i(POST("/nodes/description")), descHandler::create);
    }
}
