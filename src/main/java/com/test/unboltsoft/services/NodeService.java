package com.test.unboltsoft.services;

import com.test.unboltsoft.NodeCreatedEvent;
import com.test.unboltsoft.entities.NodeDesc;
import com.test.unboltsoft.entities.NodeRoot;
import com.test.unboltsoft.repositories.NodeDescRepository;
import com.test.unboltsoft.repositories.NodeRootRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Log4j2
@Service
public class NodeService {

    private final ApplicationEventPublisher publisher;
    private final NodeRootRepository nodeRootRepository;
    private final NodeDescRepository nodeDescRepository;


    public NodeService(ApplicationEventPublisher publisher, NodeRootRepository nodeRootRepository, NodeDescRepository nodeDescRepository) {
        this.publisher = publisher;
        this.nodeRootRepository = nodeRootRepository;
        this.nodeDescRepository = nodeDescRepository;
    }

    public Flux<NodeRoot> all() {
        Flux<NodeRoot> nodeRootFlux = nodeRootRepository.findAll();

        return nodeRootFlux;
    }

    public Flux<NodeDesc> allDesc() {
        return nodeDescRepository.findByDescriptionIsNotNull();
    }

    public Mono<NodeRoot> create(NodeRoot node) {
        return nodeRootRepository
                .save(new NodeRoot(null, node.getName()))
                .doOnSuccess(profile -> this.publisher.publishEvent(new NodeCreatedEvent(profile)));
    }

    public Mono<NodeDesc> createDecs(NodeDesc node) {
        return nodeDescRepository
                .save(new NodeDesc(null, node.getName(), node.getDescription()))
                .doOnSuccess(profile -> this.publisher.publishEvent(new NodeCreatedEvent(profile)));
    }
}
