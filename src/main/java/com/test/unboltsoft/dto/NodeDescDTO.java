package com.test.unboltsoft.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper = true)
@Getter
public class NodeDescDTO extends NodeRootDTO {

    private String description;

    public NodeDescDTO(String id, String name, String description) {
        super(id, name);
        this.description = description;
    }

    public NodeDescDTO() {
    }
}
