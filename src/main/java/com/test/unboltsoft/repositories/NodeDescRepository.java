package com.test.unboltsoft.repositories;

import com.test.unboltsoft.entities.NodeDesc;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface NodeDescRepository extends ReactiveMongoRepository<NodeDesc, String> {
    Flux<NodeDesc> findByDescriptionIsNotNull();
}
