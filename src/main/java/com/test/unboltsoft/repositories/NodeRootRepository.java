package com.test.unboltsoft.repositories;

import com.test.unboltsoft.entities.NodeRoot;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NodeRootRepository extends ReactiveMongoRepository<NodeRoot, String> {
}
