package com.test.unboltsoft.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

@EqualsAndHashCode(callSuper = true)
@Document(collection = "node")
@Data
public class NodeDesc extends NodeRoot {

    private String description;

    public NodeDesc(String id, String name, String description) {
        super(id, name);
        this.description = description;
    }

    public NodeDesc() {
    }
}
