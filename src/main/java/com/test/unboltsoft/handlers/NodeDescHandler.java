package com.test.unboltsoft.handlers;

import java.net.URI;

import com.test.unboltsoft.dto.NodeDescDTO;
import com.test.unboltsoft.entities.NodeDesc;
import com.test.unboltsoft.services.NodeService;
import org.reactivestreams.Publisher;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class NodeDescHandler {

    private final NodeService nodeService;

    NodeDescHandler(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    private static Mono<ServerResponse> defaultWriteResponse(Publisher<NodeDescDTO> nodes) {
        return Mono
                .from(nodes)
                .flatMap(p -> ServerResponse
                        .created(URI.create("/nodes/" + p.getId()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .build()
                );
    }

    private static Mono<ServerResponse> defaultReadResponse(Publisher<NodeDescDTO> nodes) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(nodes, NodeDescDTO.class);
    }

    public Mono<ServerResponse> all(ServerRequest r) {
        return defaultReadResponse(nodeService.allDesc().map(nodeDesc -> {
            return new NodeDescDTO(nodeDesc.getId(), nodeDesc.getName(), nodeDesc.getDescription());
        }));
    }

    public Mono<ServerResponse> create(ServerRequest request) {
        Flux<NodeDescDTO> flux = request
                .bodyToFlux(NodeDescDTO.class)
                .map(nodeDesc -> {
                    return new NodeDesc(nodeDesc.getId(), nodeDesc.getName(), nodeDesc.getDescription());
                })
                .flatMap(nodeService::createDecs)
                .map(nodeDesc -> {
                    return new NodeDescDTO(nodeDesc.getId(), nodeDesc.getName(), nodeDesc.getDescription());
                });
        return defaultWriteResponse(flux);
    }
}
