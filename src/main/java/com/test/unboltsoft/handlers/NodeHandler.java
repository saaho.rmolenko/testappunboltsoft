package com.test.unboltsoft.handlers;

import java.net.URI;

import com.test.unboltsoft.dto.NodeRootDTO;
import com.test.unboltsoft.entities.NodeRoot;
import com.test.unboltsoft.services.NodeService;
import org.reactivestreams.Publisher;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class NodeHandler {

    private final NodeService nodeService;

    NodeHandler(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    private static Mono<ServerResponse> defaultWriteResponse(Publisher<NodeRootDTO> nodes) {
        return Mono
                .from(nodes)
                .flatMap(p -> ServerResponse
                        .created(URI.create("/nodes/" + p.getId()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .build()
                );
    }

    private static Mono<ServerResponse> defaultReadResponse(Publisher<NodeRootDTO> nodes) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(nodes, NodeRootDTO.class);
    }

    public Mono<ServerResponse> all(ServerRequest r) {
        return defaultReadResponse(nodeService.all().filter(node -> node.getClass().equals(NodeRoot.class)).map(node -> {
            return new NodeRootDTO(node.getId(), node.getName());
        }));
    }

    public Mono<ServerResponse> create(ServerRequest request) {
        Flux<NodeRootDTO> flux = request
                .bodyToFlux(NodeRootDTO.class)
                .map(nodeRoot -> {
                    return new NodeRoot(nodeRoot.getId(), nodeRoot.getName());
                })
                .flatMap(nodeService::create)
                .map(nodeRoot -> {
                    return new NodeRootDTO(nodeRoot.getId(), nodeRoot.getName());
                });
        return defaultWriteResponse(flux);
    }
}
