package com.test.unboltsoft;

import org.springframework.context.ApplicationEvent;

public class NodeCreatedEvent extends ApplicationEvent {

    public NodeCreatedEvent(Object source) {
        super(source);
    }
}
